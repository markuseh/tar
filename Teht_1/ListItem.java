package Teht_1;

// http://users.metropolia.fi/~kamaj/materiaalit/Tietorakenteet%20ja%20algoritmit/Pinototeutus/ListItem.java

public class ListItem {
    
	private String data;
	private ListItem next;
	
	public ListItem() {
            next = null;
	}
        
	public String getData() {
            return data;
	}
        
	public void setData(String aData){
            data = aData;
	}
        
	public void setLink(ListItem aLink){
            next = aLink;
	}
        
	public ListItem getLink(){
            return next;
	}
        
}
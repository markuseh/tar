package Teht_1;

// http://users.metropolia.fi/~kamaj/materiaalit/Tietorakenteet%20ja%20algoritmit/Pinototeutus/Stack.java

public class Stack {
	
	private ListItem top;
	private int size;
	
	public Stack() {
            top = null;
            size = 0;
	}
	
	public void push(String aData) {
            // luo lista-alkio, vie se pinon huipulle
            ListItem item = new ListItem();
            item.setData(aData);
            item.setLink(top);
                
            this.top = item;
            this.size++;
	}

	public ListItem pop() {
            // palauta huippualkio
            // päivitä linkki
            ListItem oldTop = null;
            
            if(this.top != null){
                oldTop = this.top;
                this.top = this.top.getLink();
                this.size--;
            }
            
            return oldTop;
	}
        
	public int amount() {
            return size;
	}
        
        public void printItems(){
            ListItem helper = this.top;
            while(helper != null){
                System.out.println(helper.getData());
                helper = helper.getLink();
            }
        }

}
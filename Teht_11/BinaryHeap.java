package Teht_11;

import java.util.Arrays;

public class BinaryHeap {
    
    private int[] heap;
    private int size;
    
    public BinaryHeap(int cap) {
        heap = new int[cap];
        size = 0;
    }
    
    public void insert(int data){ // Suuruusjärjestykseen (jätetään 0 alkuun)
        int muistiSpot = size+1;
        for (int i = 0; i < size+1; i++) {
            if(heap[i] > data){
                muistiSpot = i;
                break;
            }
        }
        for (int i = size; i >= muistiSpot; i--) {
            heap[i+1] = heap[i];
        }
        heap[muistiSpot] = data;
        size++;
    }
    
    public void deleteMin(int solmu){
        for (int i = solmu; i < size; i++) {
            heap[i] = heap[i+1];
        }
        size--;
    }
    
    public String toString(){
        String s = "";
        for (int i = 0; i <= size; i++) {
            s += heap[i]+" ; ";
        }
        return s;
    }
    
}

package Teht_11;

public class Main { 

    public static void main(String[] args) {
        
        BinaryHeap heap = new BinaryHeap(11);
        
        heap.insert(50);
        heap.insert(40);
        heap.insert(60);
        heap.insert(90);
        heap.insert(20);
        heap.insert(30);
        heap.insert(10);
        heap.insert(5);
        heap.insert(1);
        heap.insert(95);
        
        System.out.println(heap);
        System.out.println("Poistetaan viides solmu...");
        heap.deleteMin(5);
        System.out.println(heap);
        System.out.println("Poistetaan eka solmu...");
        heap.deleteMin(1);
        System.out.println(heap);
        System.out.println("Poistetaan viides solmu...");
        heap.deleteMin(5);
        System.out.println(heap);
        System.out.println("Poistetaan viides solmu...");
        heap.deleteMin(5);
        System.out.println(heap);
        System.out.println("Poistetaan viides solmu...");
        heap.deleteMin(5);
        System.out.println(heap);
        System.out.println("Poistetaan viides solmu...");
        heap.deleteMin(5);
        System.out.println(heap);
        
    }
    
}

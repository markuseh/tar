
package Teht_16;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        
        Random ran = new Random();
        long startTime = System.currentTimeMillis();
        long endTime;
        
        ///// TREESET TESTI ///// (Kesti aikaa 764ms)
        TreeSet ts = new TreeSet();
        
        for (int i = 0; i < 100000; i++) {
            int x = ran.nextInt(i*10+2) + i*10;
            while(ts.contains(x))
                x = ran.nextInt(i*10+2) + i*10;
            ts.add(x);
        }
        
        System.out.println(ts);
        endTime = System.currentTimeMillis() - startTime;
        System.out.println("Kesti aikaa "+endTime+"ms");
        //////////////////////////
        
        ///// BINARYTREE TESTI ///// (Kesti aikaa 7112ms)
        /*startTime = System.currentTimeMillis();
        BinaryTree tree = new BinaryTree(0);
        
        for (int i = 0; i < 100000; i++) {
            int x = ran.nextInt(i*10+2) + i*10;
            while(tree.haeSolmu(x))
                x = ran.nextInt(i*10+2) + i*10;
            tree.addSolmu(x);
        }
        
        tree.preOrder();
        endTime = System.currentTimeMillis() - startTime;
        System.out.println("Kesti aikaa "+endTime+"ms");*/
        //////////////////////////
        
    }
    
}

package Teht_17;

public class TrafficCard implements Comparable {
    int mTravellerNumber;
    String mOwnerName;
    float mBalance;
    
    public TrafficCard(int tNumber, String oName, float balance){
        mTravellerNumber = tNumber;
        mOwnerName = oName;
        mBalance = balance;
    }
    
    @Override
    public String toString(){
        return "owner: "+mOwnerName+" number: "+mTravellerNumber+" balance: "+mBalance;
    }

    @Override
    public int compareTo(Object o) {
        TrafficCard tc = (TrafficCard)o;
        
        if(mTravellerNumber==tc.mTravellerNumber){
            return 0;
        } else if(mTravellerNumber>tc.mTravellerNumber){
            return 1;
        } else {
            return -1;
        }
    }
}

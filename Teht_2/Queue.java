package Teht_2;

// Pohja tehtävä 1:stä

public class Queue {
	
	private QueueItem top;
        private QueueItem current;
	private int size;
	
	public Queue() {
            top = null;
            current = null;
            size = 0;
	}
	
	public void push(String aData) {
            QueueItem item = new QueueItem();
            item.setData(aData);
            
            if(this.top == null){
                this.top = item;
            }
            
            item.setLink(this.current);
            this.current = item;

            this.size++;
	}

	public QueueItem pop() {
            QueueItem oldTop = null;
            
            if(this.current != null && this.size > 0){
                oldTop = this.top;
                
                QueueItem helper = this.current;
                while(helper.getLink() != null){
                    this.top = helper;
                    helper = helper.getLink();
                }
                
                if(this.size == 1){
                    this.top = null;
                    this.current = null;
                } else {
                    this.top.setLink(null);
                }
                
                this.size--;
            }
            
            return oldTop;
	}
        
	public int amount() {
            return size;
	}
        
        public void printItems(){
            QueueItem helper = this.current;
            while(helper != null){
                System.out.println(helper.getData());
                helper = helper.getLink();
            }
        }

}
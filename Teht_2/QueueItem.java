package Teht_2;

// Pohja tehtävä 1:stä

public class QueueItem {
    
	private String data;
	private QueueItem next;
	
	public QueueItem() {
            next = null;
	}
        
	public String getData() {
            return data;
	}
        
	public void setData(String aData){
            data = aData;
	}
        
	public void setLink(QueueItem aLink){
            next = aLink;
	}
        
	public QueueItem getLink(){
            return next;
	}
        
}
package Teht_4;

// http://users.metropolia.fi/~kamaj/materiaalit/Tietorakenteet%20ja%20algoritmit/Pinototeutus/Stack.java

public class Stack {
	
        ListItem[] stack;
	int size;
	
	public Stack() {
            stack = new ListItem[10];
            size = 0;
	}
        
        //  palautetaan pino-iteraattori
        public StackIterator iterator() {
            return new StackIterator(this);
        }
	
	public void push(String aData) {
            // luo lista-alkio, vie se pinon huipulle
            ListItem item = new ListItem();
            item.setData(aData);
            
            if(this.size == 0)
                item.setLink(null);
            else
                item.setLink(stack[this.size-1]);
                
            this.stack[this.size] = item;
            this.size++;
	}

	public ListItem pop() {
            // palauta huippualkio
            // päivitä linkki
            ListItem oldTop = null;
            
            if(this.size == 0)
                return oldTop;
            
            if(this.stack[this.size-1] != null){
                oldTop = this.stack[this.size-1];
                this.stack[this.size-1] = this.stack[this.size-1].getLink();
                this.size--;
            }
            
            return oldTop;
	}
        
	public int amount() {
            return size;
	}
        
        public void printItems(){
            if(this.size == 0) return;
            ListItem helper = this.stack[this.size-1];
            while(helper != null){
                System.out.println(helper.getData());
                helper = helper.getLink();
            }
        }

}
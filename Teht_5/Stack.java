package Teht_5;

// http://users.metropolia.fi/~kamaj/materiaalit/Tietorakenteet%20ja%20algoritmit/Pinototeutus/Stack.java

import java.util.*;


public class Stack {
	
        private LinkedList<String> list;
	
	public Stack() {
            list = new LinkedList<String>();
	}
	
	public void push(String aData) {
            list.add(aData);
	}

	public String pop() {
            Object oldTop = null;

            Iterator itr = list.iterator();
            
            while(itr.hasNext()){
                oldTop = itr.next();
            }
            
            if(oldTop != null){
                list.remove(oldTop);
                return oldTop.toString();
            } else {
                return null;
            }
                
	}
        
	public int amount() {
            return list.size();
	}
        
        public void printItems(){
            Iterator itr = list.iterator();
            while(itr.hasNext()){
                System.out.println(itr.next());
            }
        }

}
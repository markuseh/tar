
package Teht_7;

public class BinaryTree {

    private Node root;
    public static BinaryTree found; // findWithPreOrder()-operaation apurakenne

    public BinaryTree(int rootValue) {
        root = new Node(rootValue);
    }
    
    public void addSolmu(int avain){
        if(root == null){
            root = new Node(avain);
        } else if(root.getData() > avain) { //Jos avain pienempi, laita vasemmalle
            if(root.left() == null){
                BinaryTree newTree = new BinaryTree(avain);
                root.setLeft(newTree);
                System.out.println("Lisätty vasemmalle - "+avain);
            } else {
                root.left().addSolmu(avain);
            }
        } else {  // Jos avain suurempi, laita oikealle
            if(root.right() == null){
                BinaryTree newTree = new BinaryTree(avain);
                root.setRight(newTree);
                System.out.println("Lisätty oikealle - "+avain);
            } else {
                root.right().addSolmu(avain);
            }
        }
    }
    
    public void haeSolmu(int avain){
        if(root == null){
            System.out.println("Root NULL");
        } else if(root.getData() == avain){ // Jos avain sama
            System.out.println("Löydetty avain "+avain);
        } else if(root.getData() > avain){ // Jos avain pienempi, etsi vasemmalta
            if(root.left() == null){
                System.out.println("Ei löydetty vasemmalta - "+avain);
            } else {
                System.out.println("Mennää vasemmalle...");
                root.left().haeSolmu(avain);
            }
        } else {  // Jos avain suurempi, etsi oikealata
            if(root.right() == null){
                System.out.println("Ei löydetty oikealta - "+avain);
            } else {
                System.out.println("Mennää oikealle...");
                root.right().haeSolmu(avain);
            }
        }
    }

    /*public BinaryTree(String rootValue, BinaryTree left, BinaryTree right){
        root = new Node(rootValue, left, right);
    } */

    public void preOrder() {
        if (root != null) {
            System.out.println(root.getData());
            if (root.left() != null) // pääseeekö vasemmalle?
                root.left().preOrder();
            if (root.right() != null) // pääseekö oikealle?
                root.right().preOrder();
        }

    }

    // löydetty alipuu asetetaan staattiseen muuttujaan found
    public void findWithPreOrder() {

        if (root != null) {
            System.out.print(root.getData()+ ": muokkaatko tätä?");
            if (root.left()== null)
                System.out.print(" (vasemmalla tilaa)");
            if (root.right() == null)
                System.out.println(" (oikealla tilaa)");
            char select = Lue.merkki();
            if (select =='k') {
                found = this;
                return;
            }
            if (found==null && root.left() != null) // pääseekö vasemmalle?
                root.left().findWithPreOrder();
            if (found== null && root.right() != null) // pääseekö oikealle?
                root.right().findWithPreOrder();
        }

    }
    public void setNotFound() {
        found = null;
    }
    public static BinaryTree getFound() {
        return found;
    }

    public void setLeft(BinaryTree tree) {
        root.setLeft(tree);
    }

    public void setRight(BinaryTree tree) {
        root.setRight(tree);
    }
}

package Teht_8;

public class BinaryTree {

    private Node root;
    public static BinaryTree found; // findWithPreOrder()-operaation apurakenne

    public BinaryTree(String rootValue) {
        root = new Node(rootValue);
    }
    
    public void poistaSolmu(String value){
        if(root.getData().equals(value)){
            if(root.left() == null && root.right() == null){ // Ei alipuita
                root = null;
                return;
            }

            if(root.left() != null && root.right() != null){ // Molemmat alipuut
                // oikeanpuoleisen solmun alipuun vasemmanpuoleisin solmu
                Node helperRoot = root.right().getRoot();
                Node helperRoot2 = root.right().getRoot();
                while(helperRoot.left() != null) {
                    System.out.println("while loop: "+helperRoot.getData());
                    helperRoot = helperRoot.left().getRoot();
                    if(helperRoot.left() != null)
                        helperRoot2 = helperRoot;
                }
                root.setData(helperRoot.getData());
                helperRoot2.setLeft(null);
                return;
            }

            if(root.left() != null){ // Vasen alipuu
                root = root.left().getRoot();
            } else { // Oikea alipuu
                root = root.right().getRoot();
            }
        } else {
            if (root.left() != null) // pääseeekö vasemmalle?
                root.left().poistaSolmu(value);
            if (root.right() != null) // pääseekö oikealle?
                root.right().poistaSolmu(value);
        }
    }
    
    public Node getRoot(){
        return this.root;
    }
    
    public Node getVasenRoot(){
        if (root.left() != null)
            root.left().getVasenRoot();
        return root;
    }
    
    /*public BinaryTree(String rootValue, BinaryTree left, BinaryTree right){
        root = new Node(rootValue, left, right);
    } */

    public void preOrder() {
        if (root != null) {
            System.out.println(root.getData()+',');
            if (root.left() != null) // pääseeekö vasemmalle?
                root.left().preOrder();
            if (root.right() != null) // pääseekö oikealle?
                root.right().preOrder();
        }

    }

    // löydetty alipuu asetetaan staattiseen muuttujaan found
    public void findWithPreOrder() {

        if (root != null) {
            System.out.print(root.getData()+ ": muokkaatko tätä?");
            if (root.left()== null)
                System.out.print(" (vasemmalla tilaa)");
            if (root.right() == null)
                System.out.println(" (oikealla tilaa)");
            char select = Lue.merkki();
            if (select =='k') {
                found = this;
                return;
            }
            if (found==null && root.left() != null) // pääseekö vasemmalle?
                root.left().findWithPreOrder();
            if (found== null && root.right() != null) // pääseekö oikealle?
                root.right().findWithPreOrder();
        }

    }
    public void setNotFound() {
        found = null;
    }
    public static BinaryTree getFound() {
        return found;
    }

    public void setLeft(BinaryTree tree) {
        root.setLeft(tree);
    }

    public void setRight(BinaryTree tree) {
        root.setRight(tree);
    }
}